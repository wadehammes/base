Base
======

This base structure is what I think is the best starting point (for me at least) for development of static sites, templates, prototypes and more. It is a great beginners framework for learning <a href="http://sass-lang.com">SASS</a> and an even more excellent way to learn responsive design using the amazing mixin offered by <a href="http://compass-style.org">Compass</a> and the <a href="http://gumbyframework.com/docs/grid/">Gumby Framework grid</a>. It has a small 'MVC-style' approach, <a href="http://modernizr.com">Modernizr</a> built in, and some JS libraries for IE compatibility.

In the latest update, I have included the <a href="http://retinajs.com">retina.js</a> plugin with accompanying .SCSS mixin (found in sass/mixins) which will help serve up retina images on the fly.

####PRIOR TO USAGE:

You will need to make sure you have the following installed to your machine (via Terminal):

<code>sudo gem install sass</code>
<code>sudo gem install compass</code>
<code>sudo gem install modular-scale</code>

This will be needed for when you are watching the project for SASS changes. To do so, clone Base. Copy all files from the Base clone and paste them into your new project directory. Via Terminal, navigate to your the root level of this new directory. Run the command:

<code>compass watch</code>

This will start watching for changes in your SCSS files within your project.

Start building something awesome.

####OTHER TIDBITS:
- Use _global.scss in the global sass directory for creating style guide classes and other reusuable, global elements.
- Use _screen.scss in the screen sass directory for screen styles
- Use _print.scss in the print sass directory for print styles

This template makes use of all Compass mixins (for full list and documentation see here: http://compass-style.org/)
It's foundation is the Gumby Framework grid: http://gumbyframework.com/docs/grid/

####RELEASE LOG:

<a href="https://github.com/wadehammes/Base/releases">https://github.com/wadehammes/Base/releases</a>