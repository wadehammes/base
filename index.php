<?php $page = 'index'; // Page specific class for styling ?>
<?php $pageClasses = array(
    'standard'); // Other page specific classes
?>

<?php include('include/doctype.php'); ?>

		<main>
			<div class="row">
				<div class="eight columns centered">
					<h1>Base <i class="icon-code"></i></h1>
					<p>Go build something awesome.</p>
				</div>
			</div>
		</main>

<?php include('include/footer.php'); ?>

		