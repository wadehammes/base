<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<title>Base</title>
		<meta name="description" content="" />

		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1, user-scalable=no" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

		<!-- FAVICON -->
		<link rel="icon" href="icons/fav16.png" sizes="16x16" type="image/png" />
		<link rel="icon" href="icons/fav32.png" sizes="32x32" type="image/png" />
		<link rel="icon" href="icons/fav48.png" sizes="48x48" type="image/png" />
		<link rel="icon" href="icons/fav64.png" sizes="64x64" type="image/png" />
		<link rel="icon" href="icons/fav128.png" sizes="128x128" type="image/png" />
		<link rel="icon" href="icons/fav32.png" />
		<!--[if IE]>
			<link rel="shortcut icon" href="icons/favicon.ico" />
		<![endif]-->

		<!-- SOCIAL META -->
		<meta property="og:title" content="Base Template"/>
		<meta property="og:image" content="images/_placeholder.jpg"/>
		<meta property="og:site_name" content=""/>
		<meta property="og:description" content="Base Template" />

		<!-- STYLES -->
		<link href="css/print.css" media="print" rel="stylesheet" type="text/css" />
		<link href="css/screen.css" media="screen, projection" rel="stylesheet" type="text/css" />

		<!-- MAIN SCRIPT -->
		<script type="text/javascript" src="js/main.js"></script>

		<!--[if lt IE 9]>
			<link href="css/ie.css" media="screen, projection" rel="stylesheet" type="text/css" />
		<![endif]-->

	</head>
	<body class="<?php print $page; ?> <?php echo implode(' ', $pageClasses); ?>">

<?php include('include/header.php'); ?>