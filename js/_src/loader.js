
(function($) {
	//Define the plugin's name here
	var __name = 'loader';
	window.PG.fn[__name] = function(options) { try {

		var self = this;

		self.defaults = {
			callback: function(data) {
				console.log('You need to specify a callback function when creating an instance of the plugin.')
			}
		};

		//-- init
		//-- ------------------------------------------------------
		
		var init = function() {
			// merging defaults with passed arguments
			self.options = $.extend({}, self.defaults, options);
			//-
			return self;
		};

		//-- Vars
		//-- ------------------------------------------------------

		//-- Start
		//-- ------------------------------------------------------
		var fetch = self.fetch = function(url) {
			$.ajax({
				dataType: "jsonp",
				url: url,
				success: function(data) {
					self.options.callback(data);
				},
				error: function(data) {
					err(data);
				}
			});
		};
		return init();
	}
	catch(e) {
		err(e);
	}};
})($);