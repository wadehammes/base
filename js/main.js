//* ==========================================
//- $script.js - asynchronous script loader
//* ==========================================
(function(e,t,n){if(typeof module!="undefined"&&module.exports)module.exports=n();else if(typeof define=="function"&&define.amd)define(n);else t[e]=n()})("$script",this,function(){function v(e,t){for(var n=0,r=e.length;n<r;++n)if(!t(e[n]))return f;return 1}function m(e,t){v(e,function(e){return!t(e)})}function g(e,t,a){function d(e){return e.call?e():r[e]}function b(){if(!--p){r[h]=1;c&&c();for(var e in s){v(e.split("|"),d)&&!m(s[e],d)&&(s[e]=[])}}}e=e[l]?e:[e];var f=t&&t.call,c=f?t:a,h=f?e.join(""):t,p=e.length;setTimeout(function(){m(e,function(e){if(e===null)return b();if(u[e]){h&&(i[h]=1);return u[e]==2&&b()}u[e]=1;h&&(i[h]=1);y(!n.test(e)&&o?o+e+".js":e,b)})},0);return g}function y(n,r){var i=e.createElement("script"),s=f;i.onload=i.onerror=i[d]=function(){if(i[h]&&!/^c|loade/.test(i[h])||s)return;i.onload=i[d]=null;s=1;u[n]=2;r()};i.async=1;i.src=n;t.insertBefore(i,t.firstChild)}var e=document,t=e.getElementsByTagName("head")[0],n=/^https?:\/\//,r={},i={},s={},o,u={},a="string",f=false,l="push",c="DOMContentLoaded",h="readyState",p="addEventListener",d="onreadystatechange";if(!e[h]&&e[p]){e[p](c,function b(){e.removeEventListener(c,b,f);e[h]="complete"},f);e[h]="loading"}g.get=y;g.order=function(e,t,n){(function r(i){i=e.shift();if(!e.length)g(i,t,n);else g(i,r)})()};g.path=function(e){o=e};g.ready=function(e,t,n){e=e[l]?e:[e];var i=[];!m(e,function(e){r[e]||i[l](e)})&&v(e,function(e){return r[e]})?t():!function(e){s[e]=s[e]||[];s[e][l](t);n&&n(i)}(e.join("|"));return g};g.done=function(e){g([null],e)};return g});

var $;

//* ==========================================
//- Dom ready call
var onDOMReady = function() {

	// ON READY FUNCTIONS HERE

};

//* ==========================================
//- Global Vars
(function() {
	var ua = navigator.userAgent;
	window.app = {
		'events' : {
			'click' 		: (ua.match(/iPad/i) || ua.match(/iPhone/i)) ? "touchend" : "click",
			'hoverstart' 	: (ua.match(/iPad/i) || ua.match(/iPhone/i)) ? "touchstart" : "mouseenter",
			'hoverend' 		: (ua.match(/iPad/i) || ua.match(/iPhone/i)) ? "touchmove" : "mouseleave"
		}
	};
	/*
	var scale	= ( navigator.userAgent.indexOf( 'iPhone' ) != -1 || navigator.userAgent.indexOf( 'Android' ) != -1 ? .8 : 1 );
	var width	= ( navigator.userAgent.indexOf( 'Android' ) != -1 ? '500px' : 'device-width' );
	document.write( '<meta name="viewport" content="width=' + width + ', user-scalable=yes, initial-scale=' + scale + ', minimum-scale=' + scale + ', maximum-scale=' + scale + '" />' );
	*/

	//- Path
	var s = document.getElementsByTagName("script")
	, parts = parseURI(s[0].src);

	window.PG = {
		config : {
			path: {
				js: parts['directory']
			}
		},
		fn: {},
		utils: {
			undf: function(v) {
				return typeof(v) == 'undefined';
			}
		}
	};
})();

//* ==========================================
//- Load Libs
(function () {
	//- Environment variables and references
	var _dev = true;

	//  * * *
	//- Libraries
	var aLibs = {
		'fonts' 	:
			'', // Typekit Unique URI Here
		'jquery' 	: 
	    	//window.path['root'] 	+ 'wp-includes/js/jquery/jquery.js?ver=1.8.3',
	    	'//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js',
		'vendors' 	: [
	    	//window.path['assets'] 	+ 'js/vendor/modernizr-2.6.2.min.js',
	    	PG.config.path.js 	+ 'vendor/html5shiv.js'
	    ],
	    'app' 		: _dev ? [
			//- Plugins
			PG.config.path.js 	+ '_lib/retina.js',
			PG.config.path.js 	+ 'plugins.min.js',
			//- Custom plugins
			PG.config.path.js 	+ '_src/loader.js',
		] : [
			PG.config.path.js 	+ 'plugins.min.js',
			PG.config.path.js 	+ 'app.min.js'
		]
	};

	// TYPEKIT HELPER
    $script(aLibs.fonts, 'fonts', function() {
    	try{
    		Typekit.load();
    	}
    	catch(e) {

    	}
    });
    $script(aLibs.vendors, 'vendors');
	$script(aLibs.jquery, 'jquery', function() {
		//console.log('jquery loaded!');
		$ = jQuery;
		$(document).ready(function() {
			//console.log('jquery ready');
			$script(aLibs.app, 'app', function() {
				//console.log('app loaded!');
				onDOMReady();
			});
		});
	});
})();

//* ==========================================
//- Some libs
(function(){var e;var t=function(){};var n=["assert","clear","count","debug","dir","dirxml","error","exception","group","groupCollapsed","groupEnd","info","log","markTimeline","profile","profileEnd","table","time","timeEnd","timeStamp","trace","warn"];var r=n.length;var i=window.console=window.console||{};while(r--){e=n[r];if(!i[e]){i[e]=t}}})();
// UTILS
if(typeof String.prototype.trim!=="function"){String.prototype.trim=function(){return this.replace(/^\s+|\s+$/g,"")}}
function ucfirst(e){e+="";var t=e.charAt(0).toUpperCase();return t+e.substr(1)};function capitalize(e){e+="";return e.replace(/\w\S*/g,function(e){return e.charAt(0).toUpperCase()+e.substr(1).toLowerCase()})};function lowerize(e){e+="";return e.toLowerCase()};function trim(e){try{e+="";return typeof e == 'string' ? e.trim() : e}catch(e) {dumpError(e);}}
function parseURI(e){var t={options:{strictMode:true,key:["source","protocol","authority","userInfo","user","password","host","port","relative","path","directory","file","query","anchor"],q:{name:"queryKey",parser:/(?:^|&)([^&=]*)=?([^&]*)/g},parser:{strict:/^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,loose:/^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/)?((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/}}};var n=t.options,r=n.parser[n.strictMode?"strict":"loose"].exec(e),i={},s=14;while(s--)i[n.key[s]]=r[s]||"";i[n.q.name]={};i[n.key[12]].replace(n.q.parser,function(e,t,r){if(t)i[n.q.name][t]=r});return i}
var err = function(e){
	if(typeof e==="object"){if(e.message){console.log("\nMessage: "+e.message)}if(e.stack){console.log("\nStacktrace:");console.log("====================");console.log(e.stack)}}else{console.log("dumpError :: argument is not an object")}
};